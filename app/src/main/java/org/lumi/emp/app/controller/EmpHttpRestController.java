package org.lumi.emp.app.controller;

import org.lumi.emp.app.model.Infra;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.logging.Logger;

/**
 * Created by John Tsantilis
 */

@RestController
public class EmpHttpRestController {
    @GetMapping("/status")
    @ResponseBody
    public Infra sayHello() {
        return new Infra(pingHost("mysql", 3306, 5000),
                pingHost("redis", 6379, 5000),
                pingHost("kafka", 9092, 5000));

    }

    private static boolean pingHost(String host, int port, int timeout) {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(host, port), timeout);
            LOGGER.info("It replied successfully...");

            return true;

        } catch (IOException e) {
            LOGGER.info("It did not reply successfully...");

            return false; // Either timeout or unreachable or failed DNS lookup.

        }

    }

    //Logger
    private static final Logger LOGGER = Logger.getLogger(EmpHttpRestController.class.getName());
}
