package org.lumi.emp.app.model;

public class Infra {
    @Override
    public String toString() {
        return "Infra{" +
                "Database=" + Database +
                ", Cache=" + Cache +
                ", Messaging=" + Messaging +
                '}';

    }

    public boolean isDatabase() {
        return Database;

    }

    public void setDatabase(boolean database) {
        Database = database;

    }

    public boolean isCache() {
        return Cache;

    }

    public void setCache(boolean cache) {
        Cache = cache;

    }

    public boolean isMessaging() {
        return Messaging;

    }

    public void setMessaging(boolean messaging) {
        Messaging = messaging;

    }

    public Infra(boolean database, boolean cache, boolean messaging) {
        Database = database;
        Cache = cache;
        Messaging = messaging;

    }

    private boolean Database;
    private boolean Cache;
    private boolean Messaging;

}