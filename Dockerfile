FROM openjdk:11-jdk

WORKDIR /opt/app
COPY /app/target/*.jar emp-app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-Xmx256m","-jar","emp-app.jar"]
